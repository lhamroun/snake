#ifndef SNAKE_H
# define SNAKE_H

# include <stdlib.h>
# include <stdio.h>
# include "mlx.h"
# include "libft.h"

# define WHITE 0xffffff
# define GREEN 0x28CC3C
# define RED 0xD71C27
# define BLUE 0x2260ED
# define YELLOW 0xF9D72C
# define W "\033[0m"
# define G "\033[32m"
# define R "\033[31m"
# define C "\033[36m"
# define J "\033[33m"

# define TITLE "SHNAKE"
# define WIN_X 350
# define WIN_Y 400
# define IMG_X 350
# define IMG_Y 350
# define PAS 20
# define X (IMG_X / PAS)
# define Y (IMG_Y / PAS)
# define WAIT 50000000

# define KEY_LEFT 123
# define KEY_RIGHT 124
# define KEY_DOWN 125
# define KEY_UP 126
# define KEY_SPACE 49
# define KEY_ESC 53

# define LEFT 1
# define RIGHT 2
# define DOWN 3
# define UP 4

typedef struct	s_cor
{
	int		x;
	int		y;
	int		cpt;
	int		direction;
}				t_cor;

typedef struct	s_env
{
	int		i;
	int		p;
	int		n;
	int		size;
	int		time;
	int		xm;
	int		ym;
	int		tmp;
	int		pause;
	int		bpp;
	int		sl;
	int		edn;
	void	*mlx;
	void	*win;
	void	*img;
	int		*data;
	t_cor	c[X * Y];

}				t_env;

void		init_cor(t_env *env);
void		init_mlx(t_env *env);
void		init_game(t_env *env);
int			loop_event(t_env *env);
int			key_event(int key, t_env *env);
void		move_left(t_env *env);
void		move_right(t_env *env);
void		move_down(t_env *env);
void		move_up(t_env *env);
void		pause_menu(t_env *env);
void		fill_image(t_env *env, int i);
void		color_square(t_env *env, int pos, int color);
void		generate_food(t_env *env);
void		miam(t_env *env);
int			check_snake_node(t_env *env, int i);
void		ko_function(t_env *env);
int			pos_to_pxl(t_env *env, int pos, int color);
void		loop_left(t_env *env, int i);
void		loop_right(t_env *env, int i);
void		loop_down(t_env *env, int i);
void		loop_up(t_env *env, int i);
void		body_follow(t_env *env, int i);
void		prints(t_env *env, int i);
void		wait_speed(void);
void		info_pannel(t_env *env, int i);

#endif
