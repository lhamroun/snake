#include "snake.h"

void	wait_speed(void)
{
	int		i;

	i = 0;
	while (i < WAIT)
		++i;
}

int		loop_event(t_env *env)
{
//	printf("loop1\n");
	wait_speed();
	info_pannel(env, 0);
//	printf("loop2\n");
	if (env->c[0].direction == LEFT)
		loop_left(env, 0);
	else if (env->c[0].direction == RIGHT)
		loop_right(env, 0);
	else if (env->c[0].direction == DOWN)
		loop_down(env, 0);
	else if (env->c[0].direction == UP)
		loop_up(env, 0);
	if (env->c[0].x == env->xm && env->c[0].y == env->ym)
		miam(env);
	//	printf("loop3\n");
	if (env->size > 4 && check_snake_node(env, env->size - 1) == 0)
		ko_function(env);
	fill_image(env, 0);
	//	printf("loop4\n");
	return (1);
}
//		printf("POUBELLE\n");
