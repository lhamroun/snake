#include "snake.h"

void	info_pannel(t_env *env, int i)
{
	mlx_clear_window(env->mlx, env->win);
	mlx_string_put(env->mlx, env->win, IMG_X - 120, 15, GREEN, "SCORE :");
	mlx_string_put(env->mlx, env->win, IMG_X - 45, 15, GREEN, ft_itoa(env->size - 1));
	mlx_string_put(env->mlx, env->win, 25, 15, YELLOW, TITLE);
	while (i < IMG_X)
	{
		mlx_pixel_put(env->mlx, env->win, i, 0, YELLOW);
		mlx_pixel_put(env->mlx, env->win, i, 1, YELLOW);
		mlx_pixel_put(env->mlx, env->win, i, 2, YELLOW);
		mlx_pixel_put(env->mlx, env->win, i, 48, YELLOW);
		mlx_pixel_put(env->mlx, env->win, i, 49, YELLOW);
		mlx_pixel_put(env->mlx, env->win, i, 50, YELLOW);
		++i;
	}
	i = 0;
	while (i < 100)
	{
		mlx_pixel_put(env->mlx, env->win, 0, i, YELLOW);
		mlx_pixel_put(env->mlx, env->win, 1, i, YELLOW);
		mlx_pixel_put(env->mlx, env->win, 2, i, YELLOW);
		mlx_pixel_put(env->mlx, env->win, IMG_X - 1, i, YELLOW);
		mlx_pixel_put(env->mlx, env->win, IMG_X - 2, i, YELLOW);
		mlx_pixel_put(env->mlx, env->win, IMG_X - 3, i, YELLOW);
		++i;
	}
}

int		pos_to_pxl(t_env *env, int x, int y)
{
	int		pos;

	pos = (IMG_X * y * PAS) + x * PAS;
	return (pos);
}

void	color_square(t_env *env, int pos, int color)
{
	int		pxl;

	pxl = 0;
	env->tmp = pos;
//	printf("color_square1\n");
	while (pxl < PAS * PAS)
	{
		env->tmp = 0;
		while (env->tmp < PAS)
		{
			env->data[pos] = color;
			++pos;
			++pxl;
			env->tmp++;
		}
		pos = pos + IMG_X - PAS;
	}
//	printf("color_square2\n");
}

void	fill_image(t_env *env, int i)
{
//	printf("fill_image1\n");
	ft_memset(env->data, 0, sizeof(int) * IMG_X * IMG_Y);
//	printf("fill_image2\n");
	while (i < env->size)
	{
		color_square(env, pos_to_pxl(env, env->c[i].x, env->c[i].y), WHITE);
		++i;
	}
//	printf("fill_image3\n");
	color_square(env, pos_to_pxl(env, env->xm, env->ym), GREEN);
//	printf("fill_image4\n");
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 50);
//	printf("fill_image5\n");
}

