#include "snake.h"

void	init_game(t_env *env)
{
	generate_food(env);
	fill_image(env, 0);
//	printf("init_game1\n");
	if (mlx_loop_hook(env->mlx, loop_event, env) != 0)
		return ;
//	printf("init_game2\n");
	if (mlx_key_hook(env->win, key_event, env) == 0)
		return ;
	mlx_loop(env->mlx);
}

void	init_mlx(t_env *env)
{
	env->mlx = mlx_init();
	env->win = mlx_new_window(env->mlx, WIN_X, WIN_Y, TITLE);
	env->img = mlx_new_image(env->mlx, IMG_X, IMG_Y);
	env->data = (int *)mlx_get_data_addr(env->img, &env->bpp, &env->sl, &env->edn);
}

void	init_cor(t_env *env)
{
	env->i = 0;
	env->p = 0;
	env->n = X * Y;
	env->size = 1;
	env->tmp = 0;
	env->pause = 0;
	ft_memset(env->c, 0, sizeof(*env->c) * env->n);
	env->c[0].x = X / 2;
	env->c[0].y = Y / 2;
	env->c[0].direction = RIGHT;
}
