#include "snake.h"

void	loop_left(t_env *env, int i)
{
	body_follow(env, env->size - 1);
	if (env->c[0].x > 0)
		env->c[0].x--;
	else
		env->c[0].x = X - 1;
	env->c[0].direction = LEFT;
}

void	loop_right(t_env *env, int i)
{
	body_follow(env, env->size - 1);
	if (env->c[i].x < X - 1)
		env->c[i++].x++;
	else
		env->c[i++].x = 0;
	env->c[0].direction = RIGHT;
}

void	loop_down(t_env *env, int i)
{
	body_follow(env, env->size - 1);
	if (env->c[i].y < Y - 1)
		env->c[i++].y++;
	else
		env->c[i++].y = 0;
	env->c[0].direction = DOWN;
}

void	loop_up(t_env *env, int i)
{
	body_follow(env, env->size - 1);
	if (env->c[i].y > 0)
		env->c[i++].y--;
	else
		env->c[i++].y = Y - 1;
	env->c[0].direction = UP;
}
