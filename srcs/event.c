#include "snake.h"

void	pause_menu(t_env *env)
{
	env->pause == 0 ? env->pause++ : env->pause--;
	while (1)
	{
		if (env->pause == 0)
			break ;
		if (mlx_key_hook(env->mlx, key_event, env) == 0)
			return ;
	}
}

int		key_event(int key, t_env *env)
{
//	printf("key_event1\n");
//	printf("keycode --> %d\n", key);
	if (key == KEY_LEFT && env->c[0].direction != RIGHT)
	{
//		move_left(env);
		loop_left(env, 0);
	}
	else if (key == KEY_RIGHT && env->c[0].direction != LEFT)
	{
//		move_right(env);
		loop_right(env, 0);
	}
	else if (key == KEY_DOWN && env->c[0].direction != UP)
	{
//		move_down(env);
		loop_down(env, 0);
	}
	else if (key == KEY_UP && env->c[0].direction != DOWN)
	{
//		move_up(env);
		loop_up(env, 0);
	}
	else if (key == KEY_ESC)
	{
		ft_putendl("Your Score :");
		ft_putnbr(env->size - 1);
		exit(0);
	}
	else if (key == KEY_SPACE)
		while(1);
//		pause_menu(env);
	if (env->c[0].x == env->xm && env->c[0].y == env->ym)
		miam(env);
//	printf("key_event2\n");
	fill_image(env, 0);
//	printf("key_event3\n");
	return (1);
}
