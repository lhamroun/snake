#include "snake.h"

int		main(void)
{
	t_env	env;

	init_cor(&env);
	init_mlx(&env);
	init_game(&env);
	return (1);
}
