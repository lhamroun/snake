#include "snake.h"

void	body_follow(t_env *env, int i)
{
	while (i > 0)
	{
		if (env->c[i].cpt > 0)
		{
			env->c[i].cpt--;
			--i;
			continue ;
		}
		if (env->c[i - 1].direction == LEFT)
		{
			if (env->c[i].x > 0)
				env->c[i].x--;
			else
				env->c[i].x = X - 1;
			env->c[i].direction = LEFT;
		}
		else if (env->c[i - 1].direction == RIGHT)
		{
			if (env->c[i].x < X - 1)
				env->c[i].x++;
			else
				env->c[i].x = 0;
			env->c[i].direction = RIGHT;
		}
		else if (env->c[i - 1].direction == DOWN)
		{
			if (env->c[i].y < Y - 1)
				env->c[i].y++;
			else
				env->c[i].y = 0;
			env->c[i].direction = DOWN;
		}
		else if (env->c[i - 1].direction == UP)
		{
			if (env->c[i].y > 0)
				env->c[i].y--;
			else
				env->c[i].y = Y - 1;
			env->c[i].direction = UP;
		}
		--i;
	}
}

int		check_snake_node(t_env *env, int i)
{
	while (i > 0)
	{
		if (env->p == 1)
		{
			env->p = 0;
			--i;
			continue ;
		}
		if (env->c[0].x == env->c[i].x && env->c[0].y == env->c[i].y)
			return (0);
		--i;
	}
	return (1);
}

void	ko_function(t_env *env)
{
	int		i;

	i = 15;
	ft_putendl("-------------");
	ft_putendl("-------------");
	ft_putendl("-------------\nYour score : ");
	ft_putnbr(env->size - 1);
	ft_putendl("\n-------------");
	ft_putendl("-------------");
	ft_putendl("-------------");
//	mlx_clear_window(env->mlx, env->win);
//	ft_memset(env->data, 0, sizeof(int) * IMG_X * IMG_Y);
//	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 100);
//	mlx_string_put(env->mlx, env->win, 50, IMG_Y / 2, YELLOW, "YOU LOSE !");
	while (i > 0)
	{
		wait_speed();
		--i;
	}
	exit(0);
}
