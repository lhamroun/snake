#include "snake.h"

void	generate_food(t_env *env)
{
	int		i;

	i = 0;
//	printf("generate_food1\n");
	env->xm = ft_abs(rand() % X);
	env->ym = ft_abs(rand() % Y);
	while (i < env->size)
	{
		if (env->xm == env->c[i].x && env->ym == env->c[i].y)
		{
			generate_food(env);
			i = 0;
		}
		++i;
	}
//	printf("xm -->%d\n", env->xm);
//	printf("ym -->%d\n", env->ym);
}

void	miam(t_env *env)
{
	env->p = 1;
	env->size++;
	env->c[env->size - 1].cpt = env->size - 1;
	env->c[env->size - 1].x = env->xm;
	env->c[env->size - 1].y = env->ym;
	generate_food(env);
//	prints(env, 0);
}
