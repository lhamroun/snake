#include "snake.h"

void	move_left(t_env *env)
{
	int		i;

	i = 0;
	env->c[0].direction = LEFT;
	if (env->c[i].x > 0)
		env->c[i++].x--;
	else
		env->c[i++].x = X - 1;
}

void	move_right(t_env *env)
{
	int		i;

	i = 0;
	env->c[0].direction = RIGHT;
	if (env->c[i].x < X)
		env->c[i].x++;
	else
		env->c[i].x = 0;
}

void	move_down(t_env *env)
{
	int		i;

	i = 0;
	env->c[0].direction = DOWN;
	if (env->c[i].y < Y - 1)
		env->c[i].y++;
	else
		env->c[i].y = 0;
}

void	move_up(t_env *env)
{
	int		i;

	i = 0;
	if (env->c[i].y > 0)
		env->c[i].y--;
	else
		env->c[i].y = Y - 1;
	env->c[i].direction = UP;
}

