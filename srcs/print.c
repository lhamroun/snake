#include "snake.h"

void	prints(t_env *env, int i)
{
	printf(W"===============================================================\n");
	printf("|\033[32m                  MIAM xm = %d & ym = %d                    \033[0m |\n", env->xm, env->ym);
	printf(C"                   size -------------> %d\n", env->size);
	printf(C"                   n    -------------> %d\n", env->n);
	printf(R"   -------------------------------------------------------\n");
	while (i < env->size)
	{
		printf(J"                  POINT %d -------> X = %d\n", i, env->c[i].x);
		printf(J"                          |\n");
		printf(J"                          |    cpt %d\n", env->c[i].cpt);
		printf(J"                          v\n");
		printf(J"                      Y = %d\n", env->c[i].y);
		env->c[i].direction == LEFT ? printf(J"                         LEFT\n") : 1;
		env->c[i].direction == RIGHT ? printf(J"                       RIGHT\n") : 1;
		env->c[i].direction == DOWN ? printf(J"                        DOWN\n") : 1;
		env->c[i].direction == UP ? printf(J"                        UP\n") : 1;
		env->c[i].direction == 0 ? printf(J"                        NO direction\n") : 1;
		printf(R"                    ---------------------\n");
		++i;
	}
	printf(W"|\033[31m   -------------------------------------------------------   \033[0m|\n");
	printf(W"===============================================================\n");
}
