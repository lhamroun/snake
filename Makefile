NAME = shnake

FLAG = #-Wall -Wextra -Werror

HEADER = includes/snake.h
INCLUDES = -I includes/ -I libft/ -I minilibx_macos/
LDLIBS = -L libft/ -lft -L minilibx_macos -lmlx -framework OpenGL -framework AppKit

SRCS_PATH = srcs/
SRCS_NAME = main.c init.c event.c move.c draw.c loop.c miam.c tools.c \
			loop_move.c print.c
SRCS = $(addprefix $(SRCS_PATH),$(SRCS_NAME))

OBJS_PATH = .objs/
OBJS_NAME = $(SRCS_NAME:.c=.o)
OBJS = $(addprefix $(OBJS_PATH),$(OBJS_NAME))

LIBFT_A = libft/libft.a
LIBMLX_A = minilibx_macos/libmlx.a

all: $(NAME)

$(NAME): create_obj_dir $(LIBFT_A) $(LIBMLX_A) $(OBJS)
	echo $(SRCS)
	echo $(OBJS)
	$(CC) $(FLAG) $(INCLUDES) $(LDLIBS) -o $(NAME) $(OBJS)

create_obj_dir:
	mkdir -p $(OBJS_PATH)

$(LIBMLX_A):
	make -C minilibx_macos/

$(LIBFT_A):
	make -C libft/

$(OBJS_PATH)%.o:$(SRCS_PATH)%.c $(HEADER)
	$(CC) $(FLAG) $(INCLUDES) $(LDLIBS) -o $@ -c $<

clean:
	$(RM) -rf $(OBJS_PATH)
	make clean -C libft/

fclean: clean
	$(RM) $(NAME)
	make fclean -C libft/
#	make clean -C minilibx_macos/

re: fclean all

.PHONY: all clean fclean re
